# p0000-sum-cubes-v2.py
# 
# Find all positive integers under 20 to a^3 + b^3 = c^3 + d^3 that
# does _not_ satisfy a + b = c + d.  In other words, drop trivial
# solutions; Output only the interesting ones!
# 
# Program written by Pooya Taherkhani
# Written in Python
# 
# Problem inspired by an example from Cracking Coding Interview, 6th ed.
# O(n^2) time complexity
#
# Do not output the trivial answers such as {{1, 10}, {1, 10}} or {{1, 10},
# {10, 1}}.  Output only nontrivial answers such as {{1, 12}, {9,
# 10}}---which makes an answer such as {{12, 1}, {10, 9}} trivial!

def trivial(a, b, sum_cubes, sum):
    for pair in sum_cubes[sum]:
        if a == pair[0] or a == pair[1]:
            return True
    return False

sum_cubes = {}
bound = 20
for a in range(bound):
    for b in range(bound):
        sum = a**3 + b**3
        if sum in sum_cubes:
            if not trivial(a, b, sum_cubes, sum):
                sum_cubes[sum].append((a, b))
        else:
            sum_cubes[sum] = [(a, b)]

for sum in sum_cubes:
    if len(sum_cubes[sum]) > 1:
        for pair in sum_cubes[sum]:
            print(pair, end=' ')
        print()
