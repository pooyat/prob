# p0000-sum_cubes-v1.py

# Find all positive integers under 10 to a^3 + b^3 = c^3 + d^3.

# Program written by Pooya Taherkhani
# Written in Python
# Problem adopted from Cracking Coding Interview, 6th ed. by GL McDowell

sum_cubes = {}
bound = 10
for a in range(bound):
    for b in range(bound):
        sum = a**3 + b**3
        if sum in sum_cubes:
            sum_cubes[sum].append((a, b))
        else:
            sum_cubes[sum] = [(a, b)]

for sum in sum_cubes:
    for pair1 in sum_cubes[sum]:
        for pair2 in sum_cubes[sum]:
            print(pair1, pair2)
    print()
