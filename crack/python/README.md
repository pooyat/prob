p0000. __Sum of Cubes I__. Find all positive integers under a given bound that satisfy $`a^3 + b^3 = c^3 + d^3`$.

p0000. __Sum of Cubes II__. Find all positive integers under a given bound that satisfy $`a^3 + b^3 = c^3 + d^3`$ and do _not_ satisfy $`a + b = c + d`$.  In other words, do not output trivial solutions.
