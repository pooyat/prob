# Coding Problems

My solution to short coding problems.

The `crack` folder contains solution to problems from _Cracking the Coding Interview, 6th ed._ by Gayle Laakmann McDowell.

The `leet` folder contains solution to problems from `leetcode.com`.