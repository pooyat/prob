# p001-sum-two-v2.py
# 
# In an array of integers, find all pairs that add up to a given number.
# 
# Written by Pooya Taherkhani
# Written in Python
# 
# A brute force search is used---performance can be improved.
# Time complexity: O(n^2)
# Keep track of indices by counting them explicitly.

a = [ -2, 6, 10, 5, 3, 4, 11]
sum = 9
idx_pairs = []
i = 0
for num1 in a[:-1]:
    j = i + 1
    for num2 in a[j:]:
        if num1 + num2 == sum:
            idx_pairs.append((i, j))
        j += 1
    i += 1
print(idx_pairs)
