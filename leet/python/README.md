p001. __Sum of two I__. In an array of integers, find all pairs of numbers that add up to a given number.  Using _C-style indexing_.

p001. __Sum of two II__. In an array of integers, find all pairs of numbers that add up to a given number.  By explicitly _counting indices_.

p001. __Sum of two III__. In an array of integers, find all pairs of numbers that add up to a given number.  Improve time complexity to _O(n lg n)_.

p001. __Sum of two IV__. In an array of integers, find all pairs of numbers that add up to a given number.  Improve time complexity to _O(n)_.  Using _two passes_ on the array.  Handles repetitive elements in the array.  Also handles multiple solutions.

p001. __Sum of two V__. In an array of integers, find all pairs of numbers that add up to a given number.  Improve time complexity to _O(n)_.  Using _one pass_ over the array.  Handles repetitive elements in the array.  Also handles multiple solutions.