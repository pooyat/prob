# p001-sum-two-v5.py
# 
# In an array of integers, find all pairs that add up to a given
# target.
# 
# Written by Pooya Taherkhani
# Written in Python
# 
# A hash map is used---best asymptotic time complexity possible!
# Time complexity: O(n)
#
# Use a hash map from each element to its index.
# Solve the problem in one pass;
# For each element in the array, right after adding it to a map of
# (element -> index), look up its difference from the target in the
# map. 
# Handle multiple solutions.

nums = [-2, 8, 6, 3, 10, 4, 4, 5, 3, 4, 11, 1]
target = 8

idcs = {}
idx_pairs = []
i = 0
for num in nums:
    num2 = target - num
    if num2 in idcs:
        idx_list = idcs[num2]
        for j in idx_list:
            idx_pairs.append((i, j))
    if num in idcs:
        idcs[num].append(i)
    else:
        idcs[num] = [i]
    i += 1

print(idx_pairs)
