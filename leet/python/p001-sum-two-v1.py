# p001-sum-two-v1.py
# 
# In an array of integers, find all pairs that add up to a given number.
# 
# Written by Pooya Taherkhani
# Written in Python
# 
# A brute force search is used---performance can be improved.
# Time complexity: O(n^2)
# C-style indexing!

a = [ -2, 6, 10, 5, 3, 4, 11]
sum = 9
n = len(a)
idx_pairs = []
for i in range(0, n - 1):
    for j in range(i + 1, n):
        if a[i] + a[j] == sum:
            idx_pairs.append((i, j))
print(idx_pairs)
