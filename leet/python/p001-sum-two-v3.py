# p001-sum-two-v3.py
# 
# In an array of integers, find all pairs that add up to a given
# number.
# 
# Written by Pooya Taherkhani
# Written in Python
# 
# A binary (bisection) search is used---performance can be improved.
# Time complexity: O(n lg n)
#
# Sroting takes O(n lg n) time.  In the for loop, iterating takes
# O(n), then searching takes (lg n), which makes the for the time
# complexity for the for loop O(n lg n).
# 
# Return indices of the elements in the original array.

from numpy import array, argsort
from bisect import bisect_left

def index(a, x, lo):
    'Locate the leftmost value exactly equal to x'
    i = bisect_left(a, x, lo)
    if i != len(a) and a[i] == x:
        return i
    raise ValueError

a = [-2, 8, 6, 10, 5, 3, 4, 11, 1]
sum = 7
idx_sorted = argsort(a)
idx_pairs = []
a.sort()
i = 0
for num1 in a[:-1]:
    num2 = sum - num1
    try:
        j = index(a, num2, i + 1)
    except ValueError:
        continue
    else:
        ii = idx_sorted[i]
        jj = idx_sorted[j]
        idx_pairs.append((ii, jj))
    finally:
        i += 1
print(idx_pairs)
