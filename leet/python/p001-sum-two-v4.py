# p001-sum-two-v4.py
# 
# In an array of integers, find all pairs that add up to a given
# number.
# 
# Written by Pooya Taherkhani
# Written in Python
# 
# A hash map is used---performance can still be improved!
# Time complexity: O(n)
#
# Use a hash map from each element to its index.
# Solve the problem in two passes;
# One pass over the array.  The second over the map of indices and elements.
# Return indices of the elements in the original array.
# Handles array input with repitative elements.
# Also handles multiple solutions.

nums = [-2, 8, 6, 3, 10, 4, 4, 5, 3, 4, 11, 1]
sum = 8
idcs = {}
i = 0
for num in nums:
    if num in idcs:
        idcs[num].append(i)
    else:
        idcs[num] = [i]
    i += 1

idx_pairs = set()
for num1, idx in idcs.items():
    num2 = sum - num1
    if num2 in idcs:
        for j in idx:
            for k in idcs[num2]:
                if not (j == k or (k, j) in idx_pairs):
                    idx_pairs |= {(j, k)}
print(idx_pairs)
